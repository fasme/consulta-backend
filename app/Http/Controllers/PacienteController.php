<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use Redirect;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacientes = Paciente::all();
        //return response()->json(["pacientes"=>$pacientes]);
        return view('pacientes.index',["pacientes"=>$pacientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $paciente = new Paciente;
        return view('pacientes.insert',["paciente"=>$paciente]);
    }

    public function insert2(Request $request)
    {
        $paciente = new Paciente;
        $paciente->nombre = $request->nombre;
        $paciente->apellido = $request->apellido;
        $paciente->correo = $request->correo;
        $paciente->estado = $request->estado;
        $paciente->password = $request->password;
        $paciente->rut = $request->rut;
        $paciente->telefono = $request->telefono;

        $paciente->save();

        return Redirect::to('pacientes');
    }


  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paciente = Paciente::find($id);
        //return $paciente;
        return view('pacientes.insert',["paciente"=>$paciente]);
        
    }

    public function update2(Request $request,$id){

        //return $id;
        $paciente = Paciente::find($id);
        $paciente->nombre = $request->nombre;
        $paciente->apellido = $request->apellido;
        $paciente->correo = $request->correo;
        $paciente->estado = $request->estado;
        $paciente->password = $request->password;
        $paciente->rut = $request->rut;
        $paciente->telefono = $request->telefono;

        $paciente->save();
        //$paciente = Paciente::find
        return Redirect::to('pacientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //return $request->id;
        //return $id;
        $paciente = Paciente::find($request->id);

        $paciente->delete();
    }
}
