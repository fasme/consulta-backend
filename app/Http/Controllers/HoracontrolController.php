<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horacontrol;
use App\Paciente;
use Redirect;

class HoracontrolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horacontroles = Horacontrol::all();


        $horas = array();

        foreach ($horacontroles as $value) {
            $paciente = Paciente::find($value->id_paciente);

            $horas[] = [
            //marca,sede,direccion,comuna,region,contacto,correo,fono
                "id_horacontrol" => $value->id_horacontrol,
                "paciente" => $paciente->nombre." ".$paciente->apellido." (".$paciente->rut.") ",
                "fecha" => $value->fecha,
                "estado" => $value->estado

                
            ];

        }
       
        return view('horacontrol.index',["horas"=> json_encode($horas)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $horacontrol = new Horacontrol;
        return view('horacontrol.insert',["horacontrol"=>$horacontrol]);
    }

    public function insert2(Request $request)
    {
        $horacontrol = new Horacontrol;
        $horacontrol->fecha = $request->fecha;
        $horacontrol->id_paciente = $request->id_paciente;
        $horacontrol->estado = $request->estado;
        $horacontrol->descripcion = $request->descripcion;

        $horacontrol->save();

        return Redirect::to('horacontrol');
    }


  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $horacontrol = Horacontrol::find($id);
        //return $horacontrol;
        return view('horacontrol.insert',["horacontrol"=>$horacontrol]);
        
    }

    public function update2(Request $request,$id){

        //return $id;
        $horacontrol = Horacontrol::find($id);
        $horacontrol->fecha = $request->fecha;
        $horacontrol->id_paciente = $request->id_paciente;
        $horacontrol->estado = $request->estado;
        $horacontrol->descripcion = $request->descripcion;

        $myTime = strtotime($horacontrol->fecha); 
        $horacontrol->fecha =  date("Y-m-d H:i:s", $myTime);

        $horacontrol->save();
        //$horacontrol = Horacontrol::find
        return Redirect::to('horacontrol');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //return $request->id;
        //return $id;
        $horacontrol = Horacontrol::find($request->id);

        $horacontrol->delete();
    }
}
