<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;

use Redirect;

class RecetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recetas = Receta::all();
       
        return view('receta.index',["recetas"=> $recetas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $receta = new Receta;
        return view('receta.insert',["receta"=>$receta]);
    }

    public function insert2(Request $request)
    {

        $receta = new Receta;
        $receta->categoria = $request->categoria;


        $photoName = time().'.'.$request->archivo->getClientOriginalExtension();
        $request->archivo->move(public_path('archivos/recetas'), $photoName);

        $receta->url = $photoName;

        $receta->save();

        return Redirect::to('receta');
    }


  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $receta = Receta::find($id);
        //return $receta;
        return view('receta.insert',["receta"=>$receta]);
        
    }

    public function update2(Request $request,$id){

        //return $id;
        $receta = Receta::find($id);
        $receta->fecha = $request->fecha;
        $receta->id_paciente = $request->id_paciente;
        $receta->estado = $request->estado;

        $receta->save();
        //$receta = Receta::find
        return Redirect::to('receta');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //return $request->id;
        //return $id;
        $receta = Receta::find($request->id);

        $receta->delete();
    }
}
