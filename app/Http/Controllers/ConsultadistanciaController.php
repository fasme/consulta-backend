<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consultadistancia;
use App\Paciente;
use Redirect;
use PDF;

class ConsultadistanciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultadistancias = Consultadistancia::all();

        $horas = array();
        foreach ($consultadistancias as $value) {
            $paciente = Paciente::find($value->id_paciente);
            $horas[] = [
            //marca,sede,direccion,comuna,region,contacto,correo,fono
                "id_consulta" => $value->id_consulta,
                "paciente" => $paciente->nombre." ".$paciente->apellido." (".$paciente->rut.") ",
                "img1" => $value->img1,
                "img2" => $value->img2,
                "resultado" => $value->resultado,
                "pagado" => $value->pagado

                
            ];
        }
       
        return view('consultadistancia.index',["consultadistancias"=> json_encode($horas)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $consultadistancia = new Consultadistancia;
        return view('consultadistancia.insert',["consultadistancia"=>$consultadistancia]);
    }

    public function insert2(Request $request)
    {

        $consultadistancia = new Consultadistancia;
        $consultadistancia->categoria = $request->categoria;

        $consultadistancia->save();

        return Redirect::to('consultadistancia');
    }


  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $consultadistancia = Consultadistancia::find($id);
        //return $consultadistancia;
        return view('consultadistancia.insert',["consultadistancia"=>$consultadistancia]);
        
    }

    public function update2(Request $request,$id){

        //return $id;
        $consultadistancia = Consultadistancia::find($id);
        $consultadistancia->fecha = $request->fecha;
        $consultadistancia->id_paciente = $request->id_paciente;
        $consultadistancia->estado = $request->estado;

        $consultadistancia->save();
        //$consultadistancia = Consultadistancia::find
        return Redirect::to('consultadistancia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //return $request->id;
        //return $id;
        $consultadistancia = Consultadistancia::find($request->id);

        $consultadistancia->delete();
    }


    public function informe($id){

        $consultadistancia = Consultadistancia::find($id);
        $consultadistancia1 = Consultadistancia::find($id);

        $horas = array();
        $paciente = Paciente::find($consultadistancia->id_paciente);
            $horas = [
            //marca,sede,direccion,comuna,region,contacto,correo,fono
                "id_consulta" => $consultadistancia->id_consulta,
                "paciente" => $paciente->nombre." ".$paciente->apellido." (".$paciente->rut.") ",
                "img1" => $consultadistancia->img1,
                "img2" => $consultadistancia->img2,
                "resultado" => $consultadistancia->resultado,
                "pagado" => $consultadistancia->pagado

                
            ];


        return view("consultadistancia.informe",["consultadistancia"=>json_encode($horas),"consultadistancia1"=>$consultadistancia1]);
    }

    public function informe2(Request $request, $id){

        $consultadistancia = Consultadistancia::find($id);

        $consultadistancia->peso = $request->peso;
        $consultadistancia->edad = $request->edad;
        $consultadistancia->fecha = $request->fecha;
        $consultadistancia->diagnostico = $request->diagnostico;
        $consultadistancia->save();

        $paciente = Paciente::find($consultadistancia->id_paciente);
        
        $diag = explode("\n", $consultadistancia->diagnostico);
        

//return view('consultadistancia.pdf',["consultadistancia"=>$consultadistancia,"paciente"=>$paciente]);
$pdf = PDF::loadView('consultadistancia.pdf',["consultadistancia"=>$consultadistancia,"paciente"=>$paciente,"diag"=>$diag]);
return $pdf->stream('invoice.pdf');

        //return $id;
    }
}
