<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Horacontrol extends Model
{

    protected $table = 'hora_control';
    protected $primaryKey = "id_horacontrol";
    public $timestamps = false;


}
