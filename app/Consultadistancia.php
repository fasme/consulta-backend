<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Consultadistancia extends Model
{

    protected $table = 'consulta';
    protected $primaryKey = "id_consulta";
    public $timestamps = false;


}
