
@extends('layouts.master')
 


@section('contenido')




 <div class="page-header position-relative">
        <h1>
  Recetas
<a class="btn  btn-success" href={{ url("receta/insert")}}>
  <i class="fa fa-plus-circle fa-2x pull-left"></i> Añadir</a> 
</h1>
 </div><!--/.page-header-->
   
 
<table id="example" class="table table-striped table-bordered table-hover">
  <thead>
          <tr>
            <th>Categoria</th>
            <th>Imágen</th>
            
           <th>Acciones</th>
            
          </tr>
        </thead>
        <tbody>
  		@foreach($recetas as $receta)
           <tr>
           <td>
              @if($receta->estado==0){{"3 colores"}} 
              @elseif($receta->estado==1) {{"2 colores"}} 
              @else {{"1 color"}}
              @endif
           </td>
		  	<td> {{ $receta->url }}</td>


  <td class="td-actions">
                       
                        


                          <!--<a class="green" href= {{ 'pacientes/update/'.$receta->id_receta }}>
                            <i class="fa fa-pencil bigger-130"></i>
                          </a>
                          -->

                          <a class="bootbox-alert" data-url={{ $receta->url }}>
                            <i class="fa fa-file-image-o bigger-130"></i>
                          </a>

                          <a class="red bootbox-confirm" data-id={{ $receta->id_receta }}>
                            <i class="fa fa-trash bigger-130"></i>
                          </a>
                       
     
                      </td>
</tr>
          @endforeach
        </tbody>
  </table>


  <script type="text/javascript">


 $(document).ready(function() {


$('#example').DataTable( {
  iDisplayLength: -1,
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "js/TableTools/swf/copy_csv_xls_pdf.swf"
        }
    } );


$(".bootbox-confirm").on(ace.click_event, function() {
  var id = $(this).data('id');
var tr = $(this).parents('tr'); 

          bootbox.confirm("Deseas Eliminar el registro "+id, function(result) {
            if(result) {
             // bootbox.alert("You are sure!");
             tr.fadeOut(1000);
             $.get("{{ url('receta/eliminar')}}",
              { id: id },
    
      function(data) {
        
      });
            }
          });
        });



$(".bootbox-alert").on(ace.click_event, function(){

  var id = $(this).data('url');
  var dialog = bootbox.dialog({
  title: '',
  message: "<img src='archivos/recetas/"+id+"' width='100%'>",
  size: 'large'

  });

});


});
 </script>




        

        


@stop

