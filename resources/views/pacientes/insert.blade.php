@extends('layouts.master')

@section('breadcrumb')
<ul class="breadcrumb">
            <li>
              <i class="icon-home home-icon"></i>
              <a href="#">Home</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>

            <li>
              <a href={{ URL::to('usuarios') }}>Usuarios</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>
            <li>Ver Usuarios</li>
          </ul><!--.breadcrumb-->

          @stop

@section('contenido')
<div class="page-header position-relative">
      <h1>Editar Paciente</h1>
  </div>
            <!--si el formulario contiene errores de validación-->
       <?php
  // si existe el usuario carga los datos
    if ($paciente->exists):
        $form_data = array('url' => 'pacientes/update/'.$paciente->id_paciente);
        $action    = 'Editar';
    else:
        $form_data = array('url' => 'pacientes/insert');
        $action    = 'Crear';        
    endif;

?>

            {{ Form::open($form_data) }}
        
    <div class="form-group">
    <label>Rut</label>
    <input type="text" name='rut' value="{{$paciente->rut}}" class="form-control"></input>
    </div>

    <div class="form-group">
    <label>Nombre</label>
    <input type="text" name='nombre' value="{{$paciente->nombre}}" class="form-control"></input>
    </div>
    
    <div class="form-group">
    <label>Apellido</label>
    <input type="text" name='apellido' value="{{$paciente->apellido}}" class="form-control"></input>
    </div>
    
    <div class="form-group">
    <label>Correo</label>
    <input type="text" name='correo' value="{{$paciente->correo}}" class="form-control"></input>
    </div>
    
    <div class="form-group">
    <label>telefono</label>
    <input type="text" name='telefono' value="{{$paciente->telefono}}" class="form-control"></input>
    </div>
    
    <div class="form-group">
    <label>Clave</label>
    <input type="password" name="password" value="{{$paciente->password}}" class="form-control"></input>
    </div>
    
    <div class="form-group">
    <label>Estado</label>
    <select name="estado" class="form-control">
      <option value='0' @if($paciente->estado == 0) {{"selected"}} @endif>Pendiente de aprobación</option>
      <option value='1' @if($paciente->estado == 1) {{"selected"}} @endif>Aceptado</option>
    </select>
    </div>
    
        <label></label>
    <input type="submit" value="Guardar" class="btn btn-info">

        {{ Form::close() }}





               @if ($errors->any())
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong>Por favor corrige los siguentes errores:</strong>
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif

@stop