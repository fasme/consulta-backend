@extends('layouts.master')

@section('breadcrumb')
<ul class="breadcrumb">
            <li>
              <i class="icon-home home-icon"></i>
              <a href="#">Home</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>

            <li>
              <a href={{ URL::to('usuarios') }}>Usuarios</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>
            <li>Ver Usuarios</li>
          </ul><!--.breadcrumb-->

          @stop

@section('contenido')
<div class="page-header position-relative">
      <h1>Generar informe</h1>
  </div>
            <!--si el formulario contiene errores de validación-->
       <?php
  // si existe el usuario carga los datos
       $consultadistancia = json_decode($consultadistancia);

    if ($consultadistancia1->exists):
        $form_data = array('url' => 'consultadistancia/informe/'.$consultadistancia->id_consulta,'files' => true);
        $action    = 'Editar';
    else:
        $form_data = array('url' => 'consultadistancia/insert','files' => true);
        $action    = 'Crear';        
    endif;


    
    
?>


            {{ Form::open($form_data) }}
        

        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="nombre" class="form-control" readonly="" value="{{$consultadistancia->paciente}}">
        </div>

        <div class="form-group">
            <label>Peso</label>
            <input type="text" name="peso" class="form-control"  value="">
        </div>

        <div class="form-group">
            <label>Edad</label>
            <input type="text" name="edad" class="form-control" value="">
        </div>

        <div class="form-group">
          <label>Fecha</label>
          <input type="date" name='fecha' class="form-control"></input>
        </div>

        <div class="form-group">
            <label>Diagnóstico</label>
            <textarea name="diagnostico" class="form-control" rows="10"></textarea>
        </div>

    





    <label></label>
    <input type="submit" value="Guardar" class="btn btn-info">

        {{ Form::close() }}





               @if ($errors->any())
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong>Por favor corrige los siguentes errores:</strong>
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif

@stop