@extends('layouts.master')

@section('breadcrumb')
<ul class="breadcrumb">
            <li>
              <i class="icon-home home-icon"></i>
              <a href="#">Home</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>

            <li>
              <a href={{ URL::to('usuarios') }}>Usuarios</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>
            <li>Ver Usuarios</li>
          </ul><!--.breadcrumb-->

          @stop

@section('contenido')
<div class="page-header position-relative">
      <h1>Editar Receta</h1>
  </div>
            <!--si el formulario contiene errores de validación-->
       <?php
  // si existe el usuario carga los datos
    if ($receta->exists):
        $form_data = array('url' => 'receta/update/'.$receta->id_receta,'files' => true);
        $action    = 'Editar';
    else:
        $form_data = array('url' => 'receta/insert','files' => true);
        $action    = 'Crear';        
    endif;

?>

            {{ Form::open($form_data) }}
        

        <div class="form-group">
            <label>Cateogría</label>
            <select name="categoria" class="form-control">
              <option value='0' @if($receta->estado == 0) {{"selected"}} @endif>3 Colores</option>
              <option value='1' @if($receta->estado == 1) {{"selected"}} @endif>2 Colores</option>
              <option value='2' @if($receta->estado == 2) {{"selected"}} @endif>1 Color</option>
            </select>
          </div>


        <div class="form-group">
        <label>Archivo</label>
        <input type="file" name='archivo' class="form-control"></input>
        </div>

    





    <label></label>
    <input type="submit" value="Guardar" class="btn btn-info">

        {{ Form::close() }}





               @if ($errors->any())
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong>Por favor corrige los siguentes errores:</strong>
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif

@stop