
@extends('layouts.master')
 


@section('contenido')




 <div class="page-header position-relative">
        <h1>
  Consulta a distancia
<a class="btn  btn-success" href={{ url("consultadistancia/insert")}}>
  <i class="fa fa-plus-circle fa-2x pull-left"></i> Añadir</a> 
</h1>
 </div><!--/.page-header-->
   
 
<table id="example" class="table table-striped table-bordered table-hover">
  <thead>
          <tr>
            <th>Paciente</th>
            <th>Imágenes</th>
            <th>Resultado</th>
            <th>Pagado</th>
           <th>Acciones</th>
            
          </tr>
        </thead>
        <tbody>
  		@foreach(json_decode($consultadistancias) as $consultadistancia)
           <tr>
           <td>
              {{$consultadistancia->paciente}}
           </td>
		  	<td>
          <a class="bootbox-alert" data-url={{ $consultadistancia->img1 }}>
          <i class="fa fa-file-image-o bigger-130"></i>
          </a>
          <a class="bootbox-alert" data-url={{ $consultadistancia->img2 }}>
          <i class="fa fa-file-image-o bigger-130"></i>
          </a>
        </td>
        <td>{{ $consultadistancia->resultado}}</td>
        <td>@if($consultadistancia->pagado == 0){{ "En espera de pago"}} @else {{ "Pagado" }} @endif</td>

  <td class="td-actions">
                       
                        


                       

                          <a class="green" href= {{ 'consultadistancia/informe/'.$consultadistancia->id_consulta }}>
                            <i class="fa fa-pencil bigger-130"></i>
                          </a>

                          <a class="red bootbox-confirm" data-id={{ $consultadistancia->id_consulta }}>
                            <i class="fa fa-trash bigger-130"></i>
                          </a>
                       
     
                      </td>
</tr>
          @endforeach
        </tbody>
  </table>


  <script type="text/javascript">


 $(document).ready(function() {


$('#example').DataTable( {
  iDisplayLength: -1,
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "js/TableTools/swf/copy_csv_xls_pdf.swf"
        }
    } );


$(".bootbox-confirm").on(ace.click_event, function() {
  var id = $(this).data('id');
var tr = $(this).parents('tr'); 

          bootbox.confirm("Deseas Eliminar el registro "+id, function(result) {
            if(result) {
             // bootbox.alert("You are sure!");
             tr.fadeOut(1000);
             $.get("{{ url('consulta/eliminar')}}",
              { id: id },
    
      function(data) {
        
      });
            }
          });
        });



$(".bootbox-alert").on(ace.click_event, function(){

  var id = $(this).data('url');
  var dialog = bootbox.dialog({
  title: '',
  message: "<img width='100%' src='archivos/consulta/"+id+"'>",
  size: 'large'

  });

});


});
 </script>




        

        


@stop

