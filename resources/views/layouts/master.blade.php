
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Consulta - Admin</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!--basic styles-->

		{{Html::style('css/bootstrap.min.css')}}
	{{Html::style('css/bootstrap-responsive.min.css')}}
	{{Html::style('css/font-awesome-4.2.0/css/font-awesome.min.css')}}
	{{Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300')}}
	{{Html::style('css/ace.min.css')}}
	{{Html::style('css/ace-responsive.min.css')}}
	{{Html::style('css/ace-skins.min.css')}}
	{{Html::style('css/chosen.css')}}
	{{Html::style('css/jquery-ui-1.10.3.custom.min.css')}}
	{{Html::style('js/TableTools/css/dataTables.tableTools.min.css')}}
	
	{{Html::style('css/bootstrap-datetimepicker.min.css')}}









{{Html::script('js/jquery-2.0.3.min.js')}}
	{{Html::script('js/bootstrap.min.js')}}
	{{Html::script('js/ace-elements.min.js')}}
	{{Html::script('js/ace.min.js')}}

{{ Html::script('js/DataTables-1.10.4/media/js/jquery.dataTables.min.js') }} 
{{ Html::script('js/chosen.jquery.min.js') }} 
{{ Html::script('js/jquery.maskedinput.min.js') }} 

{{ Html::script('js/TableTools/js/dataTables.tableTools.min.js') }} 
{{ Html::script('js/bootbox.min.js') }}


{{ Html::script('js/jquery.form-validator.min.js')}}
{{ Html::script('js/moment-with-locales.min.js')}}
{{ Html::script('js/bootstrap-datetimepicker.js')}}




		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

	<body>

		<div class="main-container container-fluid" id="menumenu">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-small btn-success">
							<i class="fa fa-tablet"></i>
						</button>

						<button class="btn btn-small btn-info">
							<i class="fa fa-pencil"></i>
						</button>

						<button class="btn btn-small btn-warning">
							<i class="fa fa-group"></i>
						</button>

						<button class="btn btn-small btn-danger">
							<i class="fa fa-cogs"></i>
						</button>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!--#sidebar-shortcuts-->

				<ul class="nav nav-list">
					<li>
						<a href="index.html">
							<i class="icon-dashboard"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
					</li>
<!--
					<li id="usuarioactive">
						<a href="#" class="">
							<i class="fa fa-user"></i>
							<span class="menu-text"> Usuarios </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>

						<ul class="submenu">
							<li>
								<a href={{ URL::to('usuarios/insert') }}>
									
									<i class="fa fa-angle-double-right"></i>

									Ingresar
								</a>

							</li>

							<li>
								<a href={{ URL::to('usuarios') }}>
									<i class="fa fa-angle-double-right"></i>
									Ver/Editar
								</a>
							</li>

							
						</ul>

					</li>

				-->


				<li id="usuarioactive">
						<a href={{url("pacientes")}} class="">
							<i class="fa fa-user"></i>
							<span class="menu-text"> Pacientes </span>
						
						</a>


					</li>
<!--
					<li id="clienteactive">
						<a href="#" class="">
							<i class="fa fa-male"></i>
							<span class="menu-text"> Clientes </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<ul class="submenu">
							<li>
								<a href={{ URL::to('cliente/insert') }}>
									<i class="fa fa-angle-double-right"></i>
									Ingresar
								</a>
							</li>

							<li>
								<a href={{ URL::to('cliente') }}>
									<i class="fa fa-angle-double-right"></i>
									Ver/Editar
								</a>
							</li>
-->

<li id="clienteactive">
						<a href={{ url("horacontrol")}} class="">
							<i class="fa fa-male"></i>
							<span class="menu-text"> Hora control </span>

							
						</a>


					</li>
<!--
				
					<li id="consumoactive">
						<a href="#" class="">
							<i class="fa fa-legal"></i>
							<span class="menu-text"> Consumo </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<ul class="submenu">
							<li>
								<a href={{ URL::to('consumo/insert0') }}>
									<i class="fa fa-angle-double-right"></i>
									Ingresar
								</a>
							</li>

							<li>
								<a href={{ URL::to('consumo') }}>
									<i class="fa fa-angle-double-right"></i>
									Ver/Editar
								</a>
							</li>

							
						</ul>
					</li>
-->

					<li id="consumoactive">
						<a href={{ url("consultadistancia")}} class="">
							<i class="fa fa-legal"></i>
							<span class="menu-text"> Consulta a distancia </span>

							
						</a>

					
					</li>

					<!--
					<li id="boletaactive">
						<a href={{url("boleta")}} class="">
							<i class="fa fa-list"></i>
							<span class="menu-text"> Boleta </span>

							
						</a>

						
					</li>

				-->

				<li id="cobroextraactive">
						<a href={{URL("receta")}} class="">
							<i class="fa fa-usd"></i>
					<span class="menu-text"> Recetas</span>

							
						</a>

						
					</li>


					<li id="cajachicaactive">
						<a href={{URL("blog")}} class="">
							<i class="fa fa-usd"></i>
					<span class="menu-text"> Blog</span>

							
						</a>

						
					</li>

					








			<li id="importaractive">
						<a href={{ url("desafiofisico")}} class="">
							<i class="fa fa-upload"></i>
							<span class="menu-text"> Desafío físico </span>

							
						</a>

					
					</li>




				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="fa fa-double-angle-left"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					@yield("breadcrumb")
					
				</div>

				<div class="page-content">
					<div class="row-fluid">
            <div class="container">
							<!--PAGE CONTENT BEGINS-->
							@yield("contenido")
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span12-->
					</div><!--/.rowl¡ fluid-->
					
				</div><!--/.page-content-->

				
			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->



		


		<!--page specific plugin scripts-->


		<!--inline scripts related to this page-->
	</body>
</html>

