@extends('layouts.master')

@section('breadcrumb')
<ul class="breadcrumb">
            <li>
              <i class="icon-home home-icon"></i>
              <a href="#">Home</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>

            <li>
              <a href={{ URL::to('usuarios') }}>Usuarios</a>

              <span class="divider">
                <i class="icon-angle-right arrow-icon"></i>
              </span>
            </li>
            <li>Ver Usuarios</li>
          </ul><!--.breadcrumb-->

          @stop

@section('contenido')
<div class="page-header position-relative">
      <h1>Editar Horas</h1>
  </div>
            <!--si el formulario contiene errores de validación-->
       <?php
  // si existe el usuario carga los datos
    if ($horacontrol->exists):
        $form_data = array('url' => 'horacontrol/update/'.$horacontrol->id_horacontrol);
        $action    = 'Editar';
    else:
        $form_data = array('url' => 'horacontrol/insert');
        $action    = 'Crear';        
    endif;


        $myTime = strtotime($horacontrol->fecha); 
        $horacontrol->fecha =  date("d-m-Y H:i", $myTime);

?>

{{$horacontrol->fecha}}
            {{ Form::open($form_data) }}
        

    <div class="form-group">
    <label>Paciente</label>
    <input type="text" name='id_paciente' value="{{$horacontrol->id_paciente}}" class="form-control"></input>
    </div>

    <div class="form-group">
    <label>Fecha</label>
    <input type="text" id="fecha" name='fecha' value="{{ $horacontrol->fecha }}" class="form-control"></input>
    </div>

    <div class="form-group">
    <label>Estado</label>
    <select name="estado" class="form-control">
      <option value='0' @if($horacontrol->estado == 0) {{"selected"}} @endif>Pendiente de confirmación</option>
      <option value='1' @if($horacontrol->estado == 1) {{"selected"}} @endif>Confirmada</option>
    </select>
    </div>

    <div class="form-group">
    <label>Descripción</label>
    <input type="text" name='descripcion' value="{{$horacontrol->descripcion}}" class="form-control"></input>
    </div>

    <label></label>
    <input type="submit" value="Guardar" class="btn btn-info">

        {{ Form::close() }}





               @if ($errors->any())
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong>Por favor corrige los siguentes errores:</strong>
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif



  <script type="text/javascript">


 $(document).ready(function() {

  $('#fecha').datetimepicker({
    locale: 'es'
  });

});
 </script>



@stop