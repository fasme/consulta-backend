<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
use App\User;
//llego 23-29  (proxima 23.39 - 23.49)
Route::get('/', function () {
    return view('login.login');
});

Route::get('/welcome', function () {
	//dd(Auth::check());
    return view('welcome');
});

Route::post('login', array('uses' => 'UsuarioController@postLogin'));


//PACIENTES
Route::get("/pacientes", array("uses"=>"PacienteController@index"));
Route::get("/pacientes/update/{id}", array("uses"=>"PacienteController@update"));
Route::post("/pacientes/update/{id}", array("uses"=>"PacienteController@update2"));
Route::get("/pacientes/eliminar", array("uses" => "PacienteController@destroy"));
Route::get("/pacientes/insert", array("uses" => "PacienteController@insert"));
Route::post("/pacientes/insert", array("uses" => "PacienteController@insert2"));


//HORA CONTROL
Route::get("/horacontrol", array("uses"=>"HoracontrolController@index"));
Route::get("/horacontrol/update/{id}", array("uses"=>"HoracontrolController@update"));
Route::post("/horacontrol/update/{id}", array("uses"=>"HoracontrolController@update2"));
Route::get("/horacontrol/eliminar", array("uses" => "HoracontrolController@destroy"));
Route::get("/horacontrol/insert", array("uses" => "HoracontrolController@insert"));
Route::post("/horacontrol/insert", array("uses" => "HoracontrolController@insert2"));


//RECETAS
Route::get("/receta", array("uses"=>"RecetaController@index"));
Route::get("/receta/update/{id}", array("uses"=>"RecetaController@update"));
Route::post("/receta/update/{id}", array("uses"=>"RecetaController@update2"));
Route::get("/receta/eliminar", array("uses" => "RecetaController@destroy"));
Route::get("/receta/insert", array("uses" => "RecetaController@insert"));
Route::post("/receta/insert", array("uses" => "RecetaController@insert2"));


//COSULTA DISTANCIA
Route::get("/consultadistancia", array("uses"=>"ConsultadistanciaController@index"));
Route::get("/consultadistancia/update/{id}", array("uses"=>"ConsultadistanciaController@update"));
Route::post("/consultadistancia/update/{id}", array("uses"=>"ConsultadistanciaController@update2"));
Route::get("/consultadistancia/eliminar", array("uses" => "ConsultadistanciaController@destroy"));
Route::get("/consultadistancia/insert", array("uses" => "ConsultadistanciaController@insert"));
Route::post("/consultadistancia/insert", array("uses" => "ConsultadistanciaController@insert2"));
Route::get("/consultadistancia/informe/{id}", array("uses" => "ConsultadistanciaController@informe"));
Route::post("/consultadistancia/informe/{id}", array("uses" => "ConsultadistanciaController@informe2"));